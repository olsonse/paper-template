# Makefile for template paper
#
# $Id$
#

BASE=paper

BSTINPUTS=":./sty:"

all : $(BASE).pdf

LATEX=latex
PDFLATEX=pdflatex


%.dvi: %.tex repeat
	$(LATEX) $<

%.pdf: %.tex repeat
	$(PDFLATEX) $<
.PHONY: repeat

bib:
	@export BSTINPUTS=$(BSTINPUTS) ; \
	for i in *.aux; do bibtex `basename $$i .aux`; done;

spell:
	aspell -t -c $(BASE).tex;

clean:
	$(RM) $(BASE).blg     $(BASE).end     $(BASE).bbl $(BASE).lof \
	      $(BASE).aux     $(BASE).dvi     $(BASE).log $(BASE).toc \
	      $(BASE).sym.aux $(BASE).sym.blg $(BASE).sym.bbl \
	      $(BASE).out     $(BASE)Notes.bib
	$(RM) figures/*-eps-converted-to.pdf



# sometimes useful for making output more "Windows-friendly":
fixup :
	gs -dSAFER -dBATCH -dNOPAUSE  -sDEVICE=pdfwrite \
	-sOutputFile=$(BASE)-new.pdf $(BASE).pdf \
	mv $(BASE)-new.pdf $(BASE).pdf

# Useful to create smaller PDF output
compressed :
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET \
	-dBATCH -sOutputFile=$(BASE)-compressed.pdf $(BASE).pdf
